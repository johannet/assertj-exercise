package de.johannet.assertj;

import java.util.List;

public class Company {

    private final String name;
    private final String location;
    private final List<Person> employees;

    public Company(String name, String location, List<Person> employees) {
        this.name = name;
        this.location = location;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<Person> getEmployees() {
        return employees;
    }
}
