package de.johannet.assertj;

import java.util.List;
import java.util.Objects;

public class Person {

    private final String name;
    private final int age;
    private final String gender;
    private final String nationality;
    private final List<String> interests;

    public Person(String name, int age, String gender, String nationality, List<String> interests) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.nationality = nationality;
        this.interests = interests;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getNationality() {
        return nationality;
    }

    public List<String> getInterests() {
        return interests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) &&
                age == person.age &&
                gender.equals(person.gender) &&
                nationality.equals(person.nationality) &&
                interests.equals(person.interests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, gender, nationality, interests);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", nationality='" + nationality + '\'' +
                ", interests=" + interests +
                '}';
    }
}
