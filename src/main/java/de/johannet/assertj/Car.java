package de.johannet.assertj;

import java.util.List;
import java.util.Objects;

public class Car {

    private final String color;
    private final int horsepower;
    private final boolean electric;
    private final List<String> extras;

    public Car(String color, int horsepower, boolean electric, List<String> extras) {
        this.color = color;
        this.horsepower = horsepower;
        this.electric = electric;
        this.extras = extras;
    }

    public String getColor() {
        return color;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public boolean isElectric() {
        return electric;
    }

    public List<String> getExtras() {
        return extras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return horsepower == car.horsepower &&
                electric == car.electric &&
                color.equals(car.color) &&
                extras.equals(car.extras);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, horsepower, electric, extras);
    }
}
