package de.johannet.assertj;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Exercises to practice usage of AssertJ during unit tests.
 * Note that correct assertions will lead to test failures as all scenarios are broken on purpose.
 * There are multiple "correct" results for each exercise.
 * After finding appropriate assertions, try fixing the test setup to get the test to be "green".
 */
public class Exercise {

    @Test
    void onlyDigits_and_single_1_inString() {
        var someDigits = "1234A567891";

        // TODO Testen dass der String nur Ziffern beinhaltet und nur eine '1'
    }

    @Test
    void numberIsNotCloseToZero() {
        var someDouble = 83.0 - 82.999;

        // TODO Testen dass die Zahl nicht näher an 0 ist als 0.1
    }

    @Test
    void personIsPresent_andLikesDucks() {
        var someOptional = Optional.of(
                new Person("Müller-Lüdenscheid", 11, "male", "Germany", List.of("Baths"))
        );

        // TODO Testen dass im Optional eine Person ist, die sich für Enten interessiert
    }

    @Test
    void onlyOneEntryContainsTheWordEnte() {
        String[] stringArray = {
                "Herr Müller-Lüdenscheid!",
                "Herr Dr. Klöbner",
                "Sie lassen jetzt sofort die Ente zu Wasser.",
                "Nein! Mit Ihnen teilt meine Ente das Wasser nicht."
        };

        // TODO Testen dass nur in einer Array Zeile/Eintrag das Wort 'Ente' vorkommt
    }

    @Test
    void mapContainsNoMales() {
        Map<String, Person> personMap = Map.of(
                "first", new Person("Müller-Lüdenscheid", 11, "male", "Germany", List.of("Baths")),
                "second", new Person("Hoppenstedt", 16, "female", "Germany", List.of("Yodeling"))
        );

        // TODO Testen dass keine Männer in der Map sind
    }

    @Test
    void dateIsBetweenTwoOthers() {
        var today = LocalDate.now();

        // TODO Testen dass heute genau zwischen dem 23.10.2020 und dem 31.12.2020 (beides exklusive) liegt
    }

    @Test
    void atLeastOneAdult_inGroupOfPeople() {
        var groupOfPeople = List.of(
                new Person("Müller-Lüdenscheid", 11, "male", "Germany", List.of("Baths")),
                new Person("Klöbner", 13, "male", "Switzerland", List.of("Ducks")),
                new Person("Lindemann", 14, "female", "Germany", List.of("Lotto")),
                new Person("Hoppenstedt", 16, "female", "Germany", List.of("Yodeling")),
                new Person("Winkelmann", 17, "male", "Germany", List.of("Mother"))
        );

        // TODO Testen dass in der Gruppe mindestens eine Person volljährig ist
    }

    @Test
    void allMalesThatLikeToBath_areFromGermany() {
        var groupOfPeople = List.of(
                new Person("Müller-Lüdenscheid", 11, "male", "Germany", List.of("Baths")),
                new Person("Klöbner", 13, "male", "Switzerland", List.of("Ducks", "Baths")),
                new Person("Lindemann", 14, "female", "Germany", List.of("Lotto")),
                new Person("Hoppenstedt", 16, "female", "Germany", List.of("Yodeling")),
                new Person("Winkelmann", 17, "male", "Germany", List.of("Mother"))
        );

        // TODO Testen dass alle Männer die gerne baden aus Germany kommen
    }

    @Test
    void throwsIOExceptionWrappingIllegalStateException() throws IOException {
        thisMethodThrows();

        // TODO Testen dass die Methode eine IOException wirft, die eine IllegalStateException als Ursache hat
    }

    @Test
    void personIsEqualToExpectedPerson_ignoringNullFields() {
        var person = new Person("Müller-Lüdenscheid", 11, "male", "Germany", List.of("Baths"));

        var expectedPerson = new Person(null, 11, null, "Germany", List.of("Baths"));

        // TODO Testen dass die Person gleich der erwarteten Person ist wenn null Felder ignoriert werden
    }

    private void thisMethodThrows() throws IOException {
        try {
            String foo = null;
            foo.toString();
        } catch (Exception e) {
            throw new IOException("Oh no!", e);
        }
    }
}
