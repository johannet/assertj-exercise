package de.johannet.assertj;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import java.util.Objects;

public class CompanyAssertions extends AbstractAssert<CompanyAssertions, Company> {

    public CompanyAssertions(Company company) {
        super(company, CompanyAssertions.class);
    }

    public static CompanyAssertions assertThat(Company actual) {
        return new CompanyAssertions(actual);
    }

    public CompanyAssertions hasName(String name) {
        isNotNull();

        if (!Objects.equals(name, actual.getName())) {
            failWithMessage("Expected company's name to be <%s> but was <%s>", name, actual.getName());
        }

        return this;
    }

    public CompanyAssertions isLocatedIn(String location) {
        isNotNull();

        if (!Objects.equals(location, actual.getLocation())) {
            failWithMessage("Expected company's location to be <%s> but was <%s>", location, actual.getLocation());
        }

        return this;
    }

    public CompanyAssertions employs(Person person) {
        isNotNull();

        Assertions.assertThat(actual.getEmployees())
                .withFailMessage("Expected company to employ <%s> but it is not included in actual employees <%s>", person, actual.getEmployees())
                .contains(person);

        return this;
    }
}
