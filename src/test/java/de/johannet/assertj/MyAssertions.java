package de.johannet.assertj;

public class MyAssertions {

    public static PersonAssertions assertThat(Person actual) {
        return new PersonAssertions(actual);
    }

    public static CompanyAssertions assertThat(Company actual) {
        return new CompanyAssertions(actual);
    }
}
