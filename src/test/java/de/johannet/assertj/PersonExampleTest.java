package de.johannet.assertj;

import org.junit.jupiter.api.Test;

import java.util.List;

import static de.johannet.assertj.MyAssertions.assertThat;

class PersonExampleTest {

    @Test
    void personIsValid() {
        // Act
        var person = new Person("Mustermann", 42, "male", "Germany", List.of("Music", "Tennis"));

        // Assert
        assertThat(person)
                .hasName("Musterfrau")
                .hasAge(23)
                .hasGender("female")
                .isFrom("Iceland");

        assertThat(person).likes("Trains");
        assertThat(person).isInterestedIn("Movies");
    }
}
