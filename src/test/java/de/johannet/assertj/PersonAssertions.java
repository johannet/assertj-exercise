package de.johannet.assertj;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import java.util.Objects;

public class PersonAssertions extends AbstractAssert<PersonAssertions, Person> {

    public PersonAssertions(Person person) {
        super(person, PersonAssertions.class);
    }

    public static PersonAssertions assertThat(Person actual) {
        return new PersonAssertions(actual);
    }

    public PersonAssertions hasName(String name) {
        isNotNull();

        if (!Objects.equals(name, actual.getName())) {
            failWithMessage("Expected person's name to be <%s> but was <%s>", name, actual.getName());
        }

        return this;
    }

    public PersonAssertions hasAge(int age) {
        isNotNull();

        if (!Objects.equals(age, actual.getAge())) {
            failWithMessage("Expected person's age to be <%d> but was <%d>", age, actual.getAge());
        }

        return this;
    }

    public PersonAssertions hasGender(String gender) {
        isNotNull();

        if (!Objects.equals(gender, actual.getGender())) {
            failWithMessage("Expected person's gender to be <%s> but was <%s>", gender, actual.getGender());
        }

        return this;
    }

    public PersonAssertions isFrom(String nationality) {
        isNotNull();

        if (!Objects.equals(nationality, actual.getNationality())) {
            failWithMessage("Expected person's nationality to be <%s> but was <%s>", nationality, actual.getNationality());
        }

        return this;
    }

    public PersonAssertions likes(String interest) {
        isNotNull();

        if (!actual.getInterests().contains(interest)) {
            failWithMessage("Expected person's interests to include <%s> but it is not included in actual interests <%s>", interest, actual.getInterests());
        }

        return this;
    }

    public PersonAssertions isInterestedIn(String interest) {
        isNotNull();

        Assertions.assertThat(actual.getInterests())
                .withFailMessage("Expected person's interests to include <%s> but it is not included in actual interests <%s>", interest, actual.getInterests())
                .contains(interest);

        return this;
    }
}
